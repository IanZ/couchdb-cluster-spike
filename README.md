# Setup CouchDB cluster with Vagrant

- To start a cluster
`vagrant up`

- To stop
`vagrant halt`

- To delete cluster
`vagrant destroy -f`

# Notes:
The folder `config-non-couchdb-user` contains configuration files and script snippets for setup and running couchdb if you would like to use your own configuration and would not able to use or change the `couchdb` user created from the RPM package.
