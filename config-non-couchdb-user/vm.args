# Each node in the system must have a unique name. These are specified through
# the Erlang -name flag, which takes the form nodename@hostname. CouchDB
# recommends the following values for this flag:
#
# 1. If this is a single node, not in a cluster, use:
#    -name couchdb@127.0.0.1
# 2. If DNS is configured for this host, use the FQDN, such as:
#    -name couchdb@my.host.domain.com
# 3. If DNS isn't configured for this host, use IP addresses only, such as:
#    -name couchdb@192.168.0.1
#
# Do not rely on tricks with /etc/hosts or libresolv to handle anything
# other than the above 3 approaches correctly.
#
# Multiple CouchDBs running on the same machine can use couchdb1@, couchdb2@,
# etc.
-name couchdb@192.168.33.13

# All nodes must share the same magic cookie for distributed Erlang to work.
# Comment out this line if you synchronized the cookies by other means (using
# the ~/.erlang.cookie file, for example).
-setcookie monster

# Tell kernel and SASL not to log anything
-kernel error_logger silent
-sasl sasl_error_logger false

# Use kernel poll functionality if supported by emulator
+K true

# Start a pool of asynchronous IO threads
+A 16

# Comment this line out to enable the interactive Erlang shell on startup
+Bd -noinput

# Force use of the smp scheduler, fixes #1296
-smp enable

# Set maximum SSL session lifetime to reap terminated replication readers
-ssl session_lifetime 300

# use customized configuration file
# specify your own file path in case you can't use the couchdb user
-couch_ini /opt/couchdb/etc/default.ini /home/vagrant/couchdb/local.ini
