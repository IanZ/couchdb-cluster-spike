# alternative ways to setup cluster

# enable each node to listen on request from other nodes
curl -X PUT http://127.0.0.1:5984/_node/couchdb@192.168.33.11/_config/chttpd/bind_address -d '"0.0.0.0"'
curl -X PUT http://127.0.0.1:5984/_node/couchdb@192.168.33.12/_config/chttpd/bind_address -d '"0.0.0.0"'
curl -X PUT http://127.0.0.1:5984/_node/couchdb@192.168.33.13/_config/chttpd/bind_address -d '"0.0.0.0"'

# add server-level admin user account and setup system databases for each node
curl -X PUT http://192.168.33.11:5984/_node/couchdb@192.168.33.11/_config/admins/admin -d '"password"'
# -> at this point, request to _cluster_setup will respond "cluster enabled"
curl -X PUT http://admin:password@192.168.33.11:5984/_users
curl -X PUT http://admin:password@192.168.33.11:5984/_replicator
curl -X PUT http://admin:password@192.168.33.11:5984/_global_changes
# -> at this point, request to _cluster_setup will respond "cluster finished"

curl -X PUT http://192.168.33.12:5984/_node/couchdb@192.168.33.12/_config/admins/admin -d '"password"'
curl -X PUT http://admin:password@192.168.33.12:5984/_users
curl -X PUT http://admin:password@192.168.33.12:5984/_replicator
curl -X PUT http://admin:password@192.168.33.12:5984/_global_changes

curl -X PUT http://192.168.33.13:5984/_node/couchdb@192.168.33.13/_config/admins/admin -d '"password"'
curl -X PUT http://admin:password@192.168.33.13:5984/_users
curl -X PUT http://admin:password@192.168.33.13:5984/_replicator
curl -X PUT http://admin:password@192.168.33.13:5984/_global_changes

# run this on 11 to join 12, 13
curl -X PUT "http://admin:password@localhost:5986/_nodes/couchdb@192.168.33.12" -d {}
curl -X PUT "http://admin:password@localhost:5986/_nodes/couchdb@192.168.33.13" -d {}

